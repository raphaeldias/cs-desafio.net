﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Interfaces.Dominio
{
    public interface IUsuarioDominio : IBaseDominio<Usuario>
    {
        Usuario Entrar(string email, string senha);
        void ValidarAcessoPorToken(string token, int id);
    }
}
