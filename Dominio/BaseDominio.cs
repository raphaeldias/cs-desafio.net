﻿using Dominio.Interfaces.Dominio;
using Dominio.Interfaces.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class BaseDominio<T> : IBaseDominio<T>
    {
        private IBaseRepositorio<T> repositorio;
        public BaseDominio(IBaseRepositorio<T> repositorio)
        {
            this.repositorio = repositorio;
        }

        public void Atualizar(T obj)
        {
            repositorio.Atualizar(obj);
        }

        public void Inserir(T obj)
        {
            repositorio.Inserir(obj);
        }

        public T ObterPorId(int id)
        {
            return repositorio.ObterPorId(id);
        }
    }
}
