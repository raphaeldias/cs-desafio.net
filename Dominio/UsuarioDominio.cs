﻿using Dominio.Interfaces.Dominio;
using Dominio.Interfaces.Repositorio;
using Dominio.Interfaces.Token;
using Entidades;
using Excecoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class UsuarioDominio : BaseDominio<Usuario>, IUsuarioDominio
    {
        private IUsuarioRepositorio repositorio;
        private IToken token;
        public UsuarioDominio(IUsuarioRepositorio repositorio, IToken token) : base(repositorio)
        {
            this.repositorio = repositorio;
            this.token = token;
        }

        public Usuario Entrar(string email, string senha)
        {
            var usuario = repositorio.ObterPorEmail(email);
            if (usuario == null)
                throw new UsuarioInvalidoException("Usuário e / ou senha inválidos");

            if (usuario.Senha != senha)
                throw new SenhaInvalidaException("Usuário e / ou senha inválidos");

            usuario.UltimoLogin = DateTime.Now;
            usuario.Token = token.ObterNovo();

            repositorio.Atualizar(usuario);

            return usuario;
        }

        public new void Inserir(Usuario usuario)
        {
            if (repositorio.ObterPorEmail(usuario.Email) != null)
                throw new EmailJaExistenteException("E-mail já existente");

            usuario.DataCriacao = DateTime.Today;
            usuario.UltimoLogin = DateTime.Now;
            usuario.DataAtualizacao = DateTime.Today;
            usuario.Token = token.ObterNovo();           

            base.Inserir(usuario);
        }

        public void ValidarAcessoPorToken(string token, int id)
        {
            var tokenLimpo = token.Replace("Bearer", "").TrimStart().TrimEnd();

            if (tokenLimpo == "")
                throw new TokenInvalidoException("Não autorizado");

            var u = repositorio.ObterPorToken(tokenLimpo);
            if (u == null)
                throw new TokenInvalidoException("Não autorizado");

            if (u.Id != id)
                throw new TokenIncompativelException("Não autorizado");

            if (DateTime.Now.AddMinutes(-30) > u.UltimoLogin)
                throw new TokenExpiradoException("Sessão expirada");
        }
    }
}
