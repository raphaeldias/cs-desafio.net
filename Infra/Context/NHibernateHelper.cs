﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Infra.Maps;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Context
{
    public class NHibernateHelper
    {
        private static ISession _instancia;
        public static ISessionFactory Factory { get; set; }
        public static ISession Instancia
        {
            get
            {
                if ((_instancia == null) || (!_instancia.IsConnected))
                {
                    var helper = new NHibernateHelper();
                    _instancia = helper.GetSession();
                }
                return _instancia;
            }
        }

        public NHibernateHelper()
        {
            var connectionString = "Data Source=SQL5018.myASP.NET;Initial Catalog=DB_9FB4D5_DesafioConcrete;User Id=DB_9FB4D5_DesafioConcrete_admin;Password=!abc123!;";
            if (Factory == null)
                Factory = Factory = Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UsuarioMap>())
                    .ExposeConfiguration(x => new SchemaUpdate(x).Execute(true, true))
                    //.ExposeConfiguration(x=> new SchemaExport(x).Execute(true, true, true))
                    .BuildSessionFactory();     
        }

        public ISession GetSession()
        {
            return Factory.OpenSession();
        }
    }
}
