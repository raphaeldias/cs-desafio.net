﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Maps
{
    public class UsuarioMap : ClassMap<Usuario>
    {
        public UsuarioMap()
        {
            Table("usuario");

            Id(x => x.Id);
            Map(x => x.Nome);
            Map(x => x.Email);
            Map(x => x.DataCriacao);
            Map(x => x.DataAtualizacao);
            Map(x => x.Senha);
            Map(x => x.UltimoLogin);
            Map(x => x.Token);
            HasMany(x => x.Telefones).Cascade.All().Not.LazyLoad();
        }
    }
}
