﻿using Dominio.Interfaces.Repositorio;
using Entidades;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repositorios
{
    public class UsuarioRepositorio : BaseRepositorio<Usuario>, IUsuarioRepositorio
    {
        public Usuario ObterPorEmail(string email)
        {
            using (var session = NHibernateHelper.Instancia)
            {
                return session.QueryOver<Usuario>().Where(x => x.Email == email).SingleOrDefault();
            }
        }

        public Usuario ObterPorToken(string token)
        {
            using (var session = NHibernateHelper.Instancia)
            {
                return session.QueryOver<Usuario>().Where(x => x.Token == token).SingleOrDefault();
            }
        }
    }
}
