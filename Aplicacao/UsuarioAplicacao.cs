﻿using Aplicacao.Interfaces;
using Dominio.Interfaces.Dominio;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacao
{
    public class UsuarioAplicacao : BaseAplicacao<Usuario>, IUsuarioAplicacao
    {
        private IUsuarioDominio dominio;
        public UsuarioAplicacao(IUsuarioDominio dominio) : base(dominio)
        {
            this.dominio = dominio;
        }

        public Usuario Entrar(string email, string senha)
        {
            return dominio.Entrar(email, senha);
        }

        public void ValidarAcessoPorToken(string token, int id)
        {
            dominio.ValidarAcessoPorToken(token, id);
        }
    }
}
