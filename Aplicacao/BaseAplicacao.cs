﻿using Aplicacao.Interfaces;
using Dominio.Interfaces.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacao
{
    public class BaseAplicacao<T> : IBaseAplicacao<T>
    {
        private IBaseDominio<T> dominio;
        public BaseAplicacao(IBaseDominio<T> dominio)
        {
            this.dominio = dominio;
        }

        public void Atualizar(T obj)
        {
            dominio.Atualizar(obj);
        }

        public void Inserir(T obj)
        {
            dominio.Inserir(obj);
        }

        public T ObterPorId(int id)
        {
            return dominio.ObterPorId(id);
        }
    }
}
