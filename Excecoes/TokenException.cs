﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class TokenException : Exception
    {
        public TokenException(string mensagem) : base(mensagem)
        {
        }
    }
}
