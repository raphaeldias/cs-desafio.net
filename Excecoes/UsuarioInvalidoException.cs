﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class UsuarioInvalidoException : UsuarioException
    {
        public UsuarioInvalidoException(string mensagem) : base(mensagem)
        {
        }
    }
}
