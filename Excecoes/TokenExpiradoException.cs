﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class TokenExpiradoException : Exception
    {
        public TokenExpiradoException(string mensagem) : base(mensagem)
        {

        }
    }
}
