﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class TokenInvalidoException : TokenException
    {
        public TokenInvalidoException(string mensagem) : base(mensagem)
        {
        }
    }
}
