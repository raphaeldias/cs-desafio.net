﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class EmailJaExistenteException : UsuarioException
    {
        public EmailJaExistenteException(string mensagem) : base(mensagem)
        {
        }
    }
}
