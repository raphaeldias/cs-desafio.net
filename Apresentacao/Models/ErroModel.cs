﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apresentacao.Models
{
    public class ErroModel
    {
        public int StatusCode { get; set; }
        public string Mensagem { get; set; }
    }
}