﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Apresentacao.Models
{
    public class UsuarioRetornoModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public Collection<TelefoneModel> Telefones { get; set; }
        public DateTime Data_criacao { get; set; }
        public DateTime Data_atualizacao { get; set; }
        public DateTime Ultimo_login { get; set; }
        public string Token { get; set; }
    }
}