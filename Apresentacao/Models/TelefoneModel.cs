﻿namespace Apresentacao.Models
{
    public class TelefoneModel
    {
        public string Numero { get; set; }
        public string DDD { get; set; }
    }
}