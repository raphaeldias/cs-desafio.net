﻿using Aplicacao.Interfaces;
using Apresentacao.Models;
using Entidades;
using Excecoes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Apresentacao.Controllers
{
    [RoutePrefix("api/usuario")]
    public class UsuarioController : ApiController
    {
        private IUsuarioAplicacao aplicacao;
        public UsuarioController(IUsuarioAplicacao aplicacao)
        {
            this.aplicacao = aplicacao;
        }

        [HttpPost]
        [Route("signup")]
        public HttpResponseMessage Cadastrar(UsuarioCadastroModel model)
        {
            try
            {
                var usuario = new Usuario() { Email = model.Email, Nome = model.Nome, Senha = model.Senha, Telefones = new Collection<Telefone>() };

                if(model.Telefones != null)
                    foreach(var t in model.Telefones)
                    {
                        usuario.Telefones.Add(new Telefone() { DDD = t.DDD, Numero = t.Numero, Usuario = usuario });
                    }                

                aplicacao.Inserir(usuario);

                var retorno = ObterModelConsultaPreenchido(usuario);
                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception e)
            {
                return CriarRetornoDeErro(e.Message);
            }
        }

        private UsuarioRetornoModel ObterModelConsultaPreenchido(Usuario usuario)
        {
            var retorno = new UsuarioRetornoModel()
            {
                Id = usuario.Id,
                Token = usuario.Token,
                Nome = usuario.Nome,
                Email = usuario.Email,
                Data_atualizacao = usuario.DataAtualizacao,
                Data_criacao = usuario.DataCriacao,
                Ultimo_login = usuario.UltimoLogin,
                Telefones = new Collection<TelefoneModel>()
            };

            foreach (var t in usuario.Telefones)
            {
                retorno.Telefones.Add(new TelefoneModel() { DDD = t.DDD, Numero = t.Numero });
            }

            return retorno;
        }

        [Route("login")]
        public HttpResponseMessage Entrar(LoginModel model)
        {
            try
            {
                var usuario = aplicacao.Entrar(model.Email, model.Senha);

                var retorno = ObterModelConsultaPreenchido(usuario);

                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception e)
            {
                if (e is SenhaInvalidaException)
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, new ErroModel() { StatusCode = 401, Mensagem = e.Message });

                return CriarRetornoDeErro(e.Message);
            }
        }

        [Route("profile")]
        public HttpResponseMessage Perfil(int id)
        {
            try
            {
                string token = HttpContext.Current.Request.Headers.GetValues("Authentication").First();
                aplicacao.ValidarAcessoPorToken(token, id);

                var usuario = aplicacao.ObterPorId(id);
                var retorno = ObterModelConsultaPreenchido(usuario);

                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception e)
            {
                return CriarRetornoDeErro(e.Message);
            }
            
        }

        private HttpResponseMessage CriarRetornoDeErro(string mensagem)
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, new ErroModel() { StatusCode = 403, Mensagem = mensagem });
        }
    }
}
