﻿using Dominio.Interfaces.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace TesteDominio.RepositorioMock
{
    public class UsuarioRepositorioMock : IUsuarioRepositorio
    {
        private List<Usuario> Lista;
        public UsuarioRepositorioMock()
        {
            Lista = new List<Usuario>();
        }

        public void Atualizar(Usuario obj)
        {
            var usuario = ObterPorId(obj.Id);
            Lista.Remove(usuario);
            Lista.Add(obj);
        }

        public void Inserir(Usuario obj)
        {
            Lista.Add(obj);
        }

        public Usuario ObterPorEmail(string email)
        {
            return Lista.FirstOrDefault(x => x.Email == email);
        }

        public Usuario ObterPorId(int id)
        {
            return Lista.FirstOrDefault(x => x.Id == id);
        }

        public Usuario ObterPorToken(string token)
        {
            return Lista.FirstOrDefault(x => x.Token == token);
        }
    }
}
