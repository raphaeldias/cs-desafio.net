﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dominio.Interfaces.Dominio;
using Dominio;
using Infra.Token;
using Entidades;
using System.Collections.ObjectModel;
using Dominio.Interfaces.Repositorio;
using Excecoes;
using TesteDominio.RepositorioMock;

namespace TesteDominio
{
    [TestClass]
    public class TesteUsuario
    {
        private IUsuarioDominio dominio;

        [TestInitialize]
        public void Setup()
        {
            dominio = new UsuarioDominio(new UsuarioRepositorioMock(), new Token());
        }

        private Usuario CriarUsuario()
        {
            return new Usuario()
            {
                Id = 1,
                Nome = "Usuário Teste",
                Email = "Teste@teste.com.br",
                Senha = "senha",
                Telefones = new Collection<Telefone>()
            };
        }

        [TestMethod]
        public void Deve_inserir_usuario_com_sucesso()
        {
            try
            {
                var usuario = CriarUsuario();

                dominio.Inserir(usuario);
                
                Assert.AreNotEqual(usuario.Token, "", "Token está vazio");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(EmailJaExistenteException))]
        public void Deve_gerar_excecao_ao_cadastrar_mesmo_email_duas_vezes()
        {
            var usuarioUm = CriarUsuario();
            var usuarioDois = CriarUsuario();
            usuarioDois.Id = 2;

            dominio.Inserir(usuarioUm);
            dominio.Inserir(usuarioDois);
        }

        [TestMethod]
        public void Deve_inserir_usuario_fazer_login_e_retornar_novo_token_diferente()
        {
            try
            {
                var usuario = CriarUsuario();

                dominio.Inserir(usuario);
                var tokenAnterior = usuario.Token;

                var usuarioLogado = dominio.Entrar(usuario.Email, usuario.Senha);

                Assert.IsNotNull(usuarioLogado);
                Assert.AreNotEqual(tokenAnterior, "", "Token está vazio");
                Assert.AreNotEqual(usuarioLogado.Token, tokenAnterior, "Token está igual");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TokenInvalidoException))]
        public void Deve_gerar_excecao_ao_fazer_acesso_sem_token()
        {
            dominio.ValidarAcessoPorToken("", 1);            
        }

        [TestMethod]
        [ExpectedException(typeof(TokenInvalidoException))]
        public void Deve_gerar_excecao_ao_fazer_acesso_com_token_invalido()
        {
            var usuario = CriarUsuario();
            dominio.Inserir(usuario);

            dominio.ValidarAcessoPorToken("abcdef", usuario.Id);            
        }

        [TestMethod]
        [ExpectedException(typeof(TokenIncompativelException))]
        public void Deve_gerar_excecao_ao_fazer_acesso_com_token_valido_para_usuario_incorreto()
        {
            var usuario = CriarUsuario();
            dominio.Inserir(usuario);

            dominio.ValidarAcessoPorToken(usuario.Token, 99);            
        }

        [TestMethod]
        [ExpectedException(typeof(TokenExpiradoException))]
        public void Deve_gerar_excecao_ao_fazer_acesso_com_token_expirado()
        {
            var usuario = CriarUsuario();

            dominio.Inserir(usuario);
            usuario.UltimoLogin = DateTime.Now.AddMinutes(-90);
            dominio.Atualizar(usuario);

            dominio.ValidarAcessoPorToken(usuario.Token, usuario.Id);
        }

        [TestMethod]
        public void Deve_fazer_acesso_sem_erro()
        {
            try
            {
                var usuario = CriarUsuario();

                dominio.Inserir(usuario);

                var tokenParaAcesso = "Bearer " + usuario.Token;

                dominio.ValidarAcessoPorToken(tokenParaAcesso, usuario.Id);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(UsuarioInvalidoException))]
        public void Deve_gerar_excecao_ao_fazer_login_com_email_inexistente()
        {
            var usuario = CriarUsuario();
            dominio.Inserir(usuario);

            dominio.Entrar("EmailInexistente@EmailInexistente.com.br", usuario.Senha);
        }

        [TestMethod]
        [ExpectedException(typeof(SenhaInvalidaException))]
        public void Deve_gerar_excecao_ao_fazer_login_com_email_correto_e_senha_errada()
        {
            var usuario = CriarUsuario();
            dominio.Inserir(usuario);

            dominio.Entrar(usuario.Email, "senhainvalida");
        }

        [TestMethod]
        public void Deve_atualizar_data_ultimo_login()
        {
            try
            {
                var usuario = CriarUsuario();

                dominio.Inserir(usuario);

                var diaAnterior = DateTime.Today.AddDays(-1);
                usuario.UltimoLogin = diaAnterior;
                dominio.Atualizar(usuario);

                Assert.AreEqual(usuario.UltimoLogin, diaAnterior, "Não foi atualizado o ultimo login para o dia anterior");

                dominio.Entrar(usuario.Email, usuario.Senha);

                Assert.AreNotEqual(usuario.UltimoLogin, diaAnterior, "Não foi atualizado o ultimo login");                
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }


    }
}
