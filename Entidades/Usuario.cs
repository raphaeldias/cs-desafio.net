﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Usuario : BaseEntidade
    {
        public virtual string Nome { get; set; }
        public virtual string Email { get; set; }
        public virtual DateTime DataCriacao { get; set; }
        public virtual DateTime DataAtualizacao { get; set; }
        public virtual DateTime UltimoLogin { get; set; }
        public virtual string Senha { get; set; }
        public virtual string Token { get; set; }
        public virtual ICollection<Telefone> Telefones { get; set; }
    }
}
