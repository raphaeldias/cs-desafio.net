﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Telefone : BaseEntidade
    {
        public virtual string Numero { get; set; }
        public virtual string DDD { get; set; }
        public virtual Usuario Usuario { get; set; }

    }
}
